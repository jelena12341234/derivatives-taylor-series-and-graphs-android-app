package com.example.myapp;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.*;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

public class SecondActivity extends AppCompatActivity {

    private LineGraphSeries<DataPoint> series1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



        double x,y;
        x=-20;

        GraphView graphView=findViewById(R.id.graphic);
        series1=new LineGraphSeries<>();
        LineGraphSeries<DataPoint> series2=new LineGraphSeries<>();
        //for(int i=0; i<500; i++){
          while(x<20){
            x=x+0.7;
           double y1=Math.sin(x);
            y=MainActivity.nonderivat.getNumericResult(x);
           // y=Math.sin(x);
            series1.appendData(new DataPoint(x,y),true,500);
            series2.appendData(new DataPoint(x,y1),true,500);
        }

        graphView.addSeries(series1);
        graphView.addSeries(series2);
    }

}
