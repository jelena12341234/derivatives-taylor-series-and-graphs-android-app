package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View.OnClickListener;

import ast.Operation;
import tokenizer.AbstractTreeBuilder;
import tokenizer.TokenizerException;

public class MainActivity extends AppCompatActivity {

    private EditText input;
    private TextView output;
    private Button calculate;
    private Button graph, taylor;
    public static Operation nonderivat;

    static {
        try {
            nonderivat = (new AbstractTreeBuilder("x")).getTree();
        } catch (TokenizerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input=(EditText)findViewById(R.id.input);
        output=(TextView)findViewById(R.id.output);
        calculate=(Button)findViewById(R.id.button);
        graph = findViewById(R.id.graph);
        taylor = findViewById(R.id.taylor);

        taylor.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), TaylorActivity.class);

                startActivity(intent);
            }
        });

        calculate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String st=input.getText().toString();
                AbstractTreeBuilder tree=new AbstractTreeBuilder(st);
             //   AbstractTreeBuilder tree1=new AbstractTreeBuilder(st);
                Operation derivat=null;
                try {
                    derivat = tree.getTree().getDerivative();
                  //  nonderivat = tree.getTree();
                } catch (TokenizerException e) {
                    e.printStackTrace();
                }
                //System.out.println(dervative.toString());
                //System.out.println(derivative.getNumericResult(8d));
              //  System.out.println(nonderivative.getNumericResult(1d));
              //  output.setText(derivative.getNumericResult(1d).toString());

                output.setText(derivat.toString());
            }
        });

        graph.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String st=input.getText().toString();
                AbstractTreeBuilder tree1=new AbstractTreeBuilder(st);
                try {
                    nonderivat = tree1.getTree();
                } catch (TokenizerException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(v.getContext(), SecondActivity.class);

                startActivity(intent);
            }
        });
    }
}
