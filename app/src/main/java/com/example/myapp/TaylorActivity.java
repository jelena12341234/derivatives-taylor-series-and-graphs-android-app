package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ast.Operation;
import ast.Taylor;
import tokenizer.AbstractTreeBuilder;
import tokenizer.TokenizerException;

public class TaylorActivity extends AppCompatActivity {
    private Button calculate;
    private EditText func, value, pow;
    private TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taylor);

        calculate=findViewById(R.id.calculate);
        func=findViewById(R.id.function);
        pow=findViewById(R.id.pow);
        value=findViewById(R.id.value);
        output=findViewById(R.id.output);


        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String function=func.getText().toString();
                AbstractTreeBuilder tree=new AbstractTreeBuilder(function);
                Operation derivat=null;
                try {
                    derivat = tree.getTree();
                    int p=Integer.parseInt(pow.getText().toString());
                    int v=Integer.parseInt(value.getText().toString());
                    Operation op=(new Taylor(derivat,v,p)).getTaylor();
                    output.setText(op.toString());
                } catch (TokenizerException e) {
                    e.printStackTrace();
                }


            }
        });


    }


}
