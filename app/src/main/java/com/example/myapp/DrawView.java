package com.example.myapp;

import android.content.Context;
import android.view.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Random;


public class DrawView extends View {

    public static final String TAG = "MyCustomView";

    private Handler mainHandler;

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        mainHandler = new Handler(context.getMainLooper());

        Thread t = new Thread() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                        mainHandler.post(myRunnable);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        };
        t.start();
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            invalidate();
        };
    };

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        Log.i(TAG, "[onSizeChanged][" + w + " x " + h + "   " + oldw + " x "
                + oldh + "]");
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.i(TAG, "[onDraw]");

        Paint p = new Paint();
        p.setColor(Color.BLACK);
        p.setStrokeWidth(5);

        Random r = new Random();

        // Draw the shadow
        canvas.drawLine(0, 0, r.nextInt(500), 20, p);
    }

}
