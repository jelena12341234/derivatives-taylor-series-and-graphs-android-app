package test;

import tokenizer.AbstractTreeBuilder;
import tokenizer.TokenizerException;
import ast.Operation;

public class MainClass {

	public static void main(String[] args) {
		AbstractTreeBuilder tree=new AbstractTreeBuilder("2x + 3");
		Operation derivative=null, nonderivative=null;
		try {
		//	derivative = tree.getTree().getDerivative();
			nonderivative = tree.getTree();
		} catch (TokenizerException e) {
			e.printStackTrace();
		}
		//System.out.println(derivative.toString());
		//System.out.println(derivative.getNumericResult(8d));
		System.out.println(nonderivative.getNumericResult(1d));
	}
}
