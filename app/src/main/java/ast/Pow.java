package ast;

public class Pow extends BinaryOperation {
	public Pow(Operation left, Operation right) {
		super(left, right);
	}

	public String toString() {
/*		if (right.getNumericResult(1d) == 0)
			return "";
		if (right.getNumericResult(1d) == 1)
			return left.toString();
	*/
		return left.toString() + "^(" + right.toString() +")";
	}

	@Override
	public Double getNumericResult(Double val) {
		return Math.pow(left.getNumericResult(val), right.getNumericResult(val));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Pow))
			return false;
		Pow abs = (Pow) o;
		return (left.equals(abs.left) && right.equals(abs.right));
	}


	@Override
	public Operation getDerivative() {

		if (right instanceof Constant) {
			double val = Double.parseDouble(right.toString());
			return new Product(new Constant(val + ""), new Pow(left, new Constant((val - 1) + "")));
		}
		else {
			Product p = new Product(right,new Log(left));
			return new Product(this,p.getDerivative());
		}
		
//		Operation firstTerm = new Pow(left, right);
//		Operation secondTerm = new Addition(new Product(right.getDerivative(), new Log(left)),
//				new Product(new Product(right, left.getDerivative()), left));
//		return new Product(firstTerm, secondTerm);
	}
}
