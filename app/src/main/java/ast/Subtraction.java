package ast;

public class Subtraction extends BinaryOperation {

	public Subtraction(Operation left, Operation right) {
		super(left, right);
	}

	public Operation getLeft(){
		return left;
	}
	
	public Operation getRight(){
		return right;
	}
	
	public String toString(){
		if(right.getNumericResult(1d) == 0) return left.toString();
		return left.toString() + "-" + right.toString();
	}

	@Override
	public Double getNumericResult(Double val) {
		return left.getNumericResult(val) - right.getNumericResult(val);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof Subtraction)) return false;
		Subtraction abs = (Subtraction) o;
		return (left.equals(abs.left) && right.equals(abs.right));
	}

	@Override
	public Operation getDerivative() {
		
	//	if(left.getDerivative().getNumericResult(1d) == 0 && right.getDerivative().getNumericResult(1d) == 0) return new Constant("0");
		if(right instanceof Constant) return left.getDerivative();
		if(left instanceof Constant) return new Negate(right.getDerivative());
		return new Subtraction(left.getDerivative(),right.getDerivative());
	}
}
