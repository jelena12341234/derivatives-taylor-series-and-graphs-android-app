package ast;

public class Product extends BinaryOperation {

	public Product(Operation left, Operation right) {
		super(left, right);
	}

	public Operation getLeft() {
		return left;
	}

	public Operation getRight() {
		return right;
	}

	public String toString() {
	if (left.equals(new Constant("1")))
			return right.toString();
		if (right.equals(new Constant("1")))
			return left.toString();
		// if("".equals(right.toString())) return left.toString();
		// if(left.getNumericResult(1d)==0.0 || right.getNumericResult(1d)==0.0) return
		// "";
		if ((left instanceof Negate) && (right instanceof Negate))
			
			return "(" + left.toString() + ")*(" + right.toString() + ")";
		if (left instanceof Negate)
			return "(" + left.toString() + ")" + "*" + right.toString();
		if (right instanceof Negate)
			return left.toString() + "*(" + right.toString() + ")";
			
		return "("+ left.toString() + ")*(" + right.toString() +")";
	}

	@Override
	public Double getNumericResult(Double val) {
		return left.getNumericResult(val) * right.getNumericResult(val);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Product))
			return false;
		Product abs = (Product) o;
		return (left.equals(abs.left) && right.equals(abs.right));
	}

	public int hashCode() {
		return 73 * (left.hashCode() + right.hashCode());
	}

	@Override
	public Operation getDerivative() {
		if (left instanceof Constant) return new Product(left, right.getDerivative());
		return new Addition(new Product(left.getDerivative(), right), new Product(left, right.getDerivative()));
	}
}
