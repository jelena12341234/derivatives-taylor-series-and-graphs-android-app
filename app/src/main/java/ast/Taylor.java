package ast;

public class Taylor {
	Operation operation;
	double value;
	int pow;

	public Taylor(Operation operation, double value, int pow) {
		super();
		this.operation = operation;
		this.value = value;
		this.pow = pow;
	}

	public Operation getTaylor() {
		Operation taylor = null;
		Operation op = this.operation;
		SimpleVar x = new SimpleVar();

		for (int i = 0; i < pow; i++) {
			if (i == 0) {
				taylor = new Constant(op.getNumericResult(value)+"");
				continue;
			}
			//	taylor = new Product(new Constant((op.getDerivative().getNumericResult(value)/factorial(i))+""),
				//		new Pow(new Subtraction(x, new Constant(value + "")), new Constant(Integer.toString(i))));
			else
				taylor = new Addition(taylor, new Product(
						new Constant((op.getDerivative().getNumericResult(value)/factorial(i))+""),
						new Pow(new Subtraction(x, new Constant(value + "")), new Constant(Integer.toString(i)))));

			op = op.getDerivative();

			// p = p + op.getDerivative().getNumericResult(value)/(factorial(i))*Math.pow(a,
			// b)
		}

		return taylor;
	}

	public int factorial(int pow) {
		if (pow == 0)
			return 1;
		int product = 1;

		for (int i = 1; i <= pow; i++) {
			product = product * i;
		}

		return product;
	}

}
