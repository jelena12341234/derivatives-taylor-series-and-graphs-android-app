package ast;

public class Addition extends BinaryOperation {

	public Addition(Operation left, Operation right) {
		super(left, right);
	}

	public Operation getLeft(){
		return left;
	}
	
	public Operation getRight(){
		return right;
	}
	
	@Override
	public String toString(){
	//	if(left.toString()=="" && right.toString()=="") return "";
	//	if(left.toString()=="") return right.toString();

	//	if( right.toString()=="") return left.toString();
	//	if(left.getNumericResult(1d)==0.0) return right.toString();
								 
		if(left instanceof Negate && right instanceof Negate) return "("+left.toString()+")+("+right.toString()+")";
		if(right instanceof Negate) return left.toString()+"+("+right.toString()+")"; 
		if(left instanceof Negate) return  "("+left.toString() +")+" + right.toString();
		
		return left.toString() + "+" + right.toString();
	}
	
	public Double getNumericResult(Double val)
	{
		return left.getNumericResult(val) + right.getNumericResult(val);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof Addition)) return false;
		Addition abs = (Addition) o;
		return (left.equals(abs.left) && right.equals(abs.right));
		
	}
	
	@Override
	public Operation getDerivative() {
		if(left instanceof Constant) return right.getDerivative();
		if(right instanceof Constant) return left.getDerivative();
		return new Addition(left.getDerivative(), right.getDerivative());
	}
}
