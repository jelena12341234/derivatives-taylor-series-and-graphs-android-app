package ast;

public interface Operation {
	Double getNumericResult(Double val);

	Operation getDerivative();

	public static Operation getDerivative(Operation op, int pow) {

		for (int i = 0; i < pow; i++) {
			op = op.getDerivative();
		}

		return op;
	}

}
